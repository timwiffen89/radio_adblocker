Radio Player web app AdBlocker
==

Introduction
--

This code has been created in order to work when listening to station on the Radio Player web app. It essentially reduces the system volume when adverts are playing and increases it again when the adverts have finished. It uses .scpt files written in Javascript (for automation) in oder to change the system volume.

There is curruntly no way to discern between adverts and continuous DJ talk.

Built to work on MacOS.

Configuration
--

### Station
The station is defined by the url in the 'radio.php. file in the `checkType` function. The default is Radio X.

### Volume
The two volume settings can be adjusted in 'lower\_volume.scpt' and 'normal\_volume.scpt' files. I find the default volumes to be fine in an office environment.

### Sound delay
The delay in reducing and increasing the volume is down to the time lag in the web app playing the audio compared to the live audio at the particular radio station. These can be adjusted in the 'radio.php' file as the `sleep()` value in the `music` and `advert` functions.

Running the script
--

The php script must be run from the Terminal, e.g. `$ php radio.php`.