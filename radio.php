<?php

	// Get json data from Radio Player
	function checkType()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"http://np.radioplayer.co.uk/qp/v3/events?rpId=255");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);

		curl_close ($ch);

		$data = json_decode($server_output);

		$type = $data->results->now->type;

		return $type;	
	}

	// If it's music
	function music()
	{
		echo "Upcoming music detected \xE2\x9D\x97" . PHP_EOL;
		echo "Setting normal volume in 20 seconds... \xF0\x9F\x94\x8A" . PHP_EOL;
		sleep(20);
		exec('osascript ~/Desktop/radio/normal_volume.scpt');
		echo "Currently playing: Music... \xF0\x9F\x8E\xB5" . PHP_EOL . PHP_EOL;
		
		while(true){
			$type = checkType();

			if($type != "PE_E"){
				// wait 8 seonds
				sleep(8);
				$type = checkType();
				
				// If it's still an ad...
				if($type != "PE_E"){
					advert();
				}
			} else {
				continue;
			}
		}
	}

	// If it's an advert
	function advert()
	{
		echo "Upcoming advert(s) detected \xE2\x9D\x97" . PHP_EOL;
		echo "Reducing volume in 32 seconds... \xF0\x9F\x94\x89" . PHP_EOL;
		sleep(32);
		exec('osascript ~/Desktop/radio/lower_volume.scpt');
		echo "Currently playing: Advert(s)... \xF0\x9F\x91\x8E" . PHP_EOL . PHP_EOL;
		
		while(true){
			$type = checkType();

			if($type == "PE_E"){
				music();
			} else {
				continue;
			}
		}
	}

	// Initial check
	$type = checkType();

	if($type){
		echo "Initial check: \033[32mComplete\033[0m. \xE2\x9C\x85	" . PHP_EOL . PHP_EOL;
	} else {
		echo "Cannot get data." . PHP_EOL;
	}

	if($type != "PE_E"){
		advert();
	} else {
		music();
	}